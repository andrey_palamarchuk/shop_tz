import React from 'react';
import './styles.scss'
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import ProductCardInBasket from "../productCardInBasket/productCardInBasket";

const ProductCard = ({data, productsInCart, addProductInCart}) => {
    const {
        name,
        imageUrl,
        descriptions,
        prise,
        id,
    } = data;
    const getStatusProduct = () => {
        const productInCart = productsInCart.some((item) => +item.id === +id);

        if (productInCart) {
            return (
                <Link to="/basket">
                    <div className='product-card_key'>
                        go to cart
                    </div>
                </Link>
            )
        }
        return (

            <div className='product-card_key' onClick={() => {
                addProductInCart({...data, quantity: 1})
            }}>
                add to cart
            </div>
        )
    };
    return (<div className='product-card'>
            <img className='product-card_img' src={imageUrl}/>
            <div className='product-card_description'>
                <p>{name}</p>
                <p>{descriptions}</p>
                <p>price: {prise} $</p>
            </div>
            {getStatusProduct()}
        </div>
    )
};
ProductCardInBasket.propTypes = {
    data: PropTypes.object,
    productsInCart: PropTypes.object,
    addProductInCart: PropTypes.func,
};
export default ProductCard
