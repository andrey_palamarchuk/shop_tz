import React from 'react';
import './styles.scss'
import {
    Link,
} from "react-router-dom";
import {connect} from "react-redux";

const Header = ({itemsInCart}) => {

    return (<div className='header-container'>
        <Link to="/basket">
            <div className='header-container_key'>
                Chart({itemsInCart.length})
            </div>
        </Link>
    </div>)
};

const mapStateToProps = store => {
    return {
        itemsInCart: store.card.itemInCart,
    }
};

export default connect(mapStateToProps,)(Header)
