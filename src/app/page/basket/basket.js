import React, {useState, useEffect} from 'react';
import './styles.scss'
import ProductCardInBasket from "../../component/productCardInBasket/productCardInBasket";
import {clearPrice, deleteAmount, setAmount, setItemsInCart} from "../../actions/actions";
import {connect} from "react-redux";
import UserInfoForm from "../../component/form/form";
import {Link} from "react-router-dom";

const Basket = ({itemsInCart, updateItemsInCart, updateAllAmount, allAmount, userInfo, clearPrice}) => {
    useEffect(() => {
        calculationOfTheAmount()
    }, []);
    const [requestJson, setRequestJson] = useState(null);
    const calculationOfTheAmount = (all = itemsInCart) => {
        let allSum = 0;
        all.forEach(({quantity, prise}) => {
            allSum = allSum + quantity * prise
        });

        updateAllAmount(allSum);

    };

    const updateItems = (quantity, id) => {
        const all = itemsInCart.filter((item) => {
            if (item.id === id) {
                if (quantity <= 0) {
                    return false
                }
                item.quantity = quantity;
            }
            return true
        });
        updateItemsInCart(all);
        calculationOfTheAmount(all)

    };
    const getProductCard = () => {

        return itemsInCart.map((item, index) => (
            <ProductCardInBasket content={item} updateItem={updateItems} key={index}/>
        ))

    };

    function getJson() {
        if (requestJson) {
            return (
                <textarea className='basket_textarea' disabled name="text">
                    {requestJson}
                </textarea>
            )
        }
    }

    const getContent = () => {
        if (itemsInCart.length) {
            return (
                <>
                    <div className='basket__container'>
                        <div className='basket__container_items'>
                            {getProductCard()}
                        </div>
                        <UserInfoForm handleSubmit={(e) => {
                            setRequestJson(JSON.stringify({body: {userInfo, itemsInCart, allAmount}}));
                            deleteAmount();
                            clearPrice();
                        }}/>
                    </div>
                    <div>price: {allAmount} $</div>
                </>
            )
        }
        return (
            <div className='basket__notItems'>
                <h1 className='basket__notItems_title'>You have no selected items</h1>
                <Link to={'/'} className='basket__notItems_link'>go to shop</Link>
                {getJson()}
            </div>
        )

    };
    return (
        <div className='basket'>
            {getContent()}

        </div>
    )
};

const mapStateToProps = store => {
    return {
        itemsInCart: store.card.itemInCart,
        allAmount: store.card.allAmount,
        userInfo: store.form
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        updateItemsInCart: (value) => dispatch(setItemsInCart(value)),
        updateAllAmount: (value) => dispatch(setAmount(value)),
        deleteAmount: () => dispatch(deleteAmount()),
        clearPrice: () => dispatch(clearPrice())
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Basket)
