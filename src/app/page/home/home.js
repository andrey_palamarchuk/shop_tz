import React from 'react';
import './styles.scss'
import ProductCard from "../../component/productCard/productCard";
import Items from "./scratch"
import {connect} from "react-redux";
import {setItemsInCart} from "../../actions/actions";

const Home = ({itemInCart, updateItemsInCart}) => {

    const updateCartList = (item) => {
        updateItemsInCart(itemInCart.concat(item));
    };
    const getItem = () => {
        return Items.products.map((item, index) => (
            <ProductCard data={item} key={index} productsInCart={itemInCart} addProductInCart={updateCartList}/>
        ))
    };
    return (<div className='home'>
        {getItem()}
    </div>)
};

const mapStateToProps = store => {
    return {
        itemInCart: store.card.itemInCart,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        updateItemsInCart: (value) => dispatch(setItemsInCart(value)),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Home)
