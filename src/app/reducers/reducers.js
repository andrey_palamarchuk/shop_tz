import {cardReducer} from "./card";
import {combineReducers} from 'redux'
import { reducer as reduxFormReducer } from 'redux-form';

export const rootReducer = combineReducers({
    card: cardReducer,
    form: reduxFormReducer,
});


