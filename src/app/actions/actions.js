export const SET_PRISE = 'SET_PRISE';
export const SET_AMOUNT = 'SET_AMOUNT';
export const DELETE_AMOUNT = 'DELETE_AMOUNT';
export const CLEAR_PRISE = 'CLEAR_PRISE';

export function setItemsInCart(item) {
    return {
        type: SET_PRISE,
        itemInCart: item,
    }
}
export function clearPrice() {
    return {
        type: CLEAR_PRISE,
        itemInCart: [],
    }
}
export function setAmount(item) {
    return {
        type: SET_AMOUNT,
        allAmount: item,
    }
}

export function deleteAmount() {
    return {
        type: DELETE_AMOUNT,
        allAmount: 0,
    }
}
